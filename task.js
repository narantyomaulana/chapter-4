// 1. buat array of object students ada 5 data cukup
const students = [
{
    name : 'Alex',
    provinsi : 'Jawa Tengah',
    age : 22,
    status : 'nikah'
},
{
    name : 'Rifki',
    provinsi : 'Jawa Timur',
    age : 20, 
    status : 'single'
},
{
    name : 'Imam',
    provinsi : 'Jawa Barat',
    age : 19,
    status : 'single'
},
{
    name : 'Oliv',
    provinsi : 'Sumatra',
    age : 23,
    status : 'nikah'
},
{
    name : 'Kasman',
    provinsi : 'Kalimantan',
    age : 22,
    status : 'nikah'
}
    
];
// console.log(students);
// 2. 3 function, 1 function nentuin provinsi kalian ada di jawa barat gak, umur kalian diatas 22 atau gak, status kalian single atau gak

// condition ? exprIfTrue : exprIfFalse
function jawaBarat (provinsi){
    return(provinsi ==  'Jawa Barat' ? provinsi : ' Bukan Provinsi Jawa Barat');
}
// console.log(jawaBarat(true));

// condition ? exprIfTrue : exprIfFalse
function umurSiswa(age) {
    return(age < 22 ? 'Dibawah 22' : 'Umur Lebih Dari 22');
}
// console.log(umurSiswa(true));

// condition ? exprIfTrue : exprIfFalse
function statusSiswa(status) {
    return(status ? 'Single' : 'Nikah');
}
// console.log(statusSiswa(false));

console.clear();
// 3. callback function untuk print hasil proses 3 function diatas.
// nama saya imam, saya tinggal di jawa baRAT, umur saya dibawah 22 tahun. dan status saya single loh. CODE buat Nilam
function lokasiSiswa(callback1, callback2, callback3) {
    for (const element of students) {
    // console.log(element);
    let lokasi = callback1(element.provinsi);
    // console.log(element.provinsi);
    let umur = callback2(element.age);
    let status = callback3(element.status);
    if (
        lokasi == 'Jawa Barat'&&
        umur == 'Dibawah 22'&&
        status == 'Single'
    ) {
        console.log(`Nama Saya adalah : ${element.name}, Saya berada di ${lokasi}, Umur Saya ${umur}, dan status saya ${status}`);
    }
  }
}
  
lokasiSiswa(jawaBarat, umurSiswa, statusSiswa);
  